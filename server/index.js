const { Server } = require("socket.io")

require('dotenv').config();

const io = new Server({
  cors: {
    origin: `http://${process.env.APP_HOST}:${process.env.APP_PORT}`,
  }
})

const UUID = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
});


const allUsersFinished = (answers, questions, numclients) => {
    
    let count = 0;
    for(let user in answers) {
        count += answers[user].length;
    }
    console.log("count", count, "questions.length", questions.length, "answers.length", Object.keys(answers).length, "numclients", numclients);
    return (count === questions.length * Object.keys(answers).length && Object.keys(answers).length === numclients)
}

/**
 * 
 * @param {"userId": [{question, body, footer}]} answers 
 * @param [id, question] questions 
 */
 const matchAnswers = (answers, questions) => {
    let users = Object.keys(answers);
    let combinations = [];
    
    
    for(let idx = 0; idx < users.length; idx++) {
        let userIdx = idx;
        let combination = [];
        for(let question of questions) {
            for(const [i, v] of users.entries()) {
                if(i === userIdx) {
                    combination.push(answers[v].find(a => a.question === question.id));
                }
            }
            
            userIdx++;
            if(userIdx >= users.length) {
                userIdx = 0;
            }
        }
        combinations.push(combination);
    }

    return combinations;
}

let questions = require('../src/questions.json');

let responses = {};

let currentSession = UUID();


io.on('connection', (socket) => {

    io.emit('numusers', {clients: socket.client.conn.server.clientsCount, session: currentSession, uuid: currentSession});

    socket.on('receive', msg => {
        if(!responses.hasOwnProperty(msg.user)) {
            responses[msg.user] = [];
        }
        responses[msg.user].push(msg);
        
        if(allUsersFinished(responses, questions, socket.client.conn.server.clientsCount)) {
            io.emit('populate', matchAnswers(responses, questions));
            responses = {};
            currentSession = UUID();
        }

        io.emit('status', responses);
    });

    

});

io.listen(process.env.SERVER_PORT);